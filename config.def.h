//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/                          /*Update Interval*/               /*Update Signal*/
	{"", "date '+%H:%M %4Y-%b-%d (%a)'",					60,		0},
	//{"Bat:", "getBattery", 30, 30},
	{"U:", "checkupdates | wc -l", 600, 1},
	{"M:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		2},
	{"V:", "volume", 30, 10},
	{"C:", "sensors | awk '/^Package/ {print $4 \"°C\"}'", 30, 11},
	{"", "df -h | grep '/$\\|home$\\|opt$' | awk '{print $NF \":\" $3\"/\"$2}' | sed \'s/\\/home/home/g; s/\\/opt/opt/g\' | xargs", 30, 12},
	{"", "ip a | grep -Eo 'inet (addr:)?([0-9]*\\.){3}[0-9]*' | grep -Eo '([0-9]*\\.){3}[0-9]*' | grep -v '127.0.0.1' | xargs;", 30, 20},

};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
