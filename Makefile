PREFIX ?= /usr/local
current_dir = $(shell pwd)
script_links = $(shell ls scripts)
CC ?= cc
LDFLAGS = -lX11

output: dwmblocks.c config.def.h config.h
	${CC}  dwmblocks.c $(LDFLAGS) -o dwmblocks
config.h:
	cp config.def.h $@
	[ ! -d "/sys/class/power_supply/BAT0" ] || sed -i 's/\/\/{"Bat/{"Bat/' config.h; # ATTENTION!!! removes double slashes in config.h file!!!


clean:
	rm -f *.o *.gch dwmblocks config.h
install: output
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	#install -m 0755 dwmblocks $(DESTDIR)$(PREFIX)/bin/dwmblocks
	ln -sf $(current_dir)/dwmblocks $(DESTDIR)$(PREFIX)/bin/.
	ln -sf $(current_dir)/scripts/* $(DESTDIR)$(PREFIX)/bin/.
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks
	for s in $(script_links); do\
		rm -rf $(DESTDIR)$(PREFIX)/bin/$$s; \
	done
